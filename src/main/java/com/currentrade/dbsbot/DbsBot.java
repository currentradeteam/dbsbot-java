package com.currentrade.dbsbot;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.jsoup.Jsoup;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DbsBot {

    private Properties prop = new Properties();
    private boolean runnablePause = false;
    private String action = "NONE";
    private WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        DbsBot bot = new DbsBot();
        bot.loadProperties();
        bot.resetOtp();
        FirefoxDriverManager.getInstance().setup();

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv, application/octet-stream");

        bot.driver = new FirefoxDriver(profile);
        bot.login();
        bot.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        bot.gotoTransactionHistory();
        bot.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebDriverWait wdWait = new WebDriverWait(bot.driver, 120);
        WebElement element = wdWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regenerateSMSOTP")));
        if (element.isDisplayed()) {
            //We are in OTP page
            element.click();
            bot.driver.findElement(By.id("regenerateSMSOTP")).click();
            bot.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            int counter = 0;
            String otp = "";
            do {
                otp = bot.getOtp();
                if (!otp.isEmpty()) {
                    break;
                }
                //try again in 5 seconds ...
                Thread.sleep(5000);
                counter++;

            } while (counter < 10);

            bot.driver.findElement(By.id("SMSLoginPin")).sendKeys(otp);
            bot.driver.findElement(By.id("submitButton")).click();
        }
        
        bot.processCsv();
        
        while (true){
            bot.gotoTransactionHistoryViaJS();
            WebDriverWait wdWaitA = new WebDriverWait(bot.driver, 1200);
            WebElement elementA = wdWaitA.until(ExpectedConditions.visibilityOfElementLocated(By.id("account_number_select")));
            if (elementA.isDisplayed()) {
                bot.processCsv();
            }
            
        }
        
    }

    private void loadProperties() {
        InputStream input = null;
        try {
            //String path = ClassLoader.getSystemClassLoader().getResource(".").getPath() + "/config.properties";
            String path = "./config.properties";
            input = new FileInputStream(path);
            // load a properties file
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getFormattedDate(String format) {
        if (format.isEmpty()) {
            format = "dd/MM/yyyy";
        }
        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(format);
        return DATE_FORMAT.format(today);

    }
    private String generateScpCommand(String csvfilepath, String newfilename) {
        String user = prop.getProperty("serveruser");
        String host = prop.getProperty("serverhost");
        String servercsvpath = prop.getProperty("servercsvpath");
        String serverport = prop.getProperty("serverport");
        String bankid = prop.getProperty("bankid");
        return "scp -P " +serverport+" "+ csvfilepath + " "+user+"@"+host+":"+servercsvpath + "/" +bankid+ "/" + newfilename;
    }
    private String getCsvFilePath(String filename) {
        return prop.getProperty("csvdownloadpath") + "/" + filename;
    }
    private String executeCommand(String command) {
        
        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    private void login() {
        // Navigate to URL
        driver.get(prop.getProperty("homeurl"));
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("document.LoginForm.submit();");
        }
        WebDriverWait wdWait = new WebDriverWait(driver, 1200);
        WebElement element = wdWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("UID")));
        if (element.isDisplayed()) {
            // Maximize the window.
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.id("UID")).sendKeys(prop.getProperty("username"));
            driver.findElement(By.id("PIN")).sendKeys(prop.getProperty("password"));
            driver.findElement(By.className("btn-primary")).click();
        }
        
    }
    private void gotoTransactionHistoryViaJS() {
        //Try to refresh transaction history page again
        String stateName = driver.findElement(By.name("statemachineStateName")).getAttribute("value");
        String serviceId = driver.findElement(By.name("SERVICE_ID")).getAttribute("value");
        String jsString = "javascript:goToState('" + serviceId + "','" + stateName+"','RetrieveTransactionHistory');";
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript(jsString);
        }
    }
    private void gotoTransactionHistory() {
        
        WebElement frameElement = driver.findElement(By.name("user_area"));
        driver.switchTo().frame(frameElement);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        WebElement frameElement2 = driver.findElement(By.name("iframe1"));
        driver.switchTo().frame(frameElement2);
        WebDriverWait wdWait = new WebDriverWait(driver, 20);
        WebElement element = wdWait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ico-accounts")));
        if (element.isDisplayed()) {
            //We are in the wallet
            element.click();
        }

    }
    private void processCsv() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
        //fill out transaction form
        Select select = new Select(driver.findElement(By.id("account_number_select")));
        select.selectByIndex(1);
        Select select2 = new Select(driver.findElement(By.name("lstSort")));
        select2.selectByValue("Credit");
        driver.findElement(By.id("date-from")).sendKeys(getFormattedDate(""));
        driver.findElement(By.id("date-to")).sendKeys(getFormattedDate(""));
        driver.findElement(By.xpath("//button[@title=\"Go\"]")).click();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("DownloadTransactions")).click();
        //get the filename
        String filename = driver.findElement(By.name("TXN_DOWNLOAD_FILE_NAME")).getAttribute("value") + ".csv";
        Thread.sleep(2000);
        String csvfilepath = getCsvFilePath(filename);
        String newfilename = getFormattedDate("yyyy-MM-dd") + prop.getProperty("bankid")+"_" + ".csv";
        File oldfile = new File(csvfilepath);
        File newfile = new File(prop.getProperty("csvdownloadpath") + "/" + newfilename);
        if (oldfile.length() != newfile.length()) {
            System.out.println("Transfer the file!");
            executeCommand(generateScpCommand(csvfilepath, newfilename));
        }
        oldfile.renameTo(newfile);
        
    }
    private void resetOtp() {
        String content = "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prop.getProperty("resetotpurl"));
        stringBuilder.append("?key=").append(prop.getProperty("key"));
        String url = stringBuilder.toString();
        try {
            content = Jsoup.connect(url).ignoreContentType(true).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getOtp() {
        String content = "";
        String url = prop.getProperty("getotpurl");
        try {
            content = Jsoup.connect(url).ignoreContentType(true).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.trim();
    }

}
